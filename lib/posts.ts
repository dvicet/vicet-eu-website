import fs from 'fs';
import path from 'path'
import matter from 'gray-matter'

interface PostFrontMatter {
  id: Array<string>,
  title: string,
  date: string,
  updatedDate?: string
}

interface PostData {
  frontMatter: PostFrontMatter,
  content: string
}

interface PostInfo {
  frontMatter: PostFrontMatter,
  category: string
}

export type { PostData, PostInfo }

class Posts {
  static postsDirectory = path.join(process.cwd(), 'posts')

  static async getAllPostIds(): Promise<Array<string>> {
    // Recursively find all posts and return their path
    const getFilesInDirectory = async (directoryPath: string) => {
      const directoryDirents: Array<fs.Dirent> = await fs.promises.readdir(
        this.postsDirectory + directoryPath,
        { withFileTypes: true }
      )
      let postPaths: Array<string> = []
      for (let dirent of directoryDirents) {
        // rename ' ' by '_' in all files and directories
        const currentName = path.join(
          this.postsDirectory,
          directoryPath,
          dirent.name
        )
        dirent.name = dirent.name.replace(/ /g, '_')
        const newName = path.join(
          this.postsDirectory,
          directoryPath,
          dirent.name
        )
        await fs.promises.rename(currentName, newName)

        // Get all posts paths
        if (dirent.isDirectory()) {
          postPaths.push(path.join(directoryPath, dirent.name)) // for index page
          for (let file of
            await getFilesInDirectory(path.join(directoryPath, dirent.name)))
            postPaths.push(file)
        } else if (dirent.isFile()) {
          postPaths.push(path.join(directoryPath, dirent.name))
        }
      }
      return postPaths
    }

    try {
      const postPaths = await getFilesInDirectory('/')
      for (let i in postPaths) {
        postPaths[i] = '/posts' + postPaths[i].replace(/.md$/, '')
      }
      return postPaths
    } catch (error) {
      console.error(error)
      throw error
    }
  }

  static async getPostData(id: Array<string>): Promise<PostData> {
    try {
      const idPath = path.join(...id)
      const filePath = path.join(this.postsDirectory, `${idPath}.md`)

      const file = await fs.promises.readFile(filePath, { encoding: 'utf-8' })
      const matterResult = matter(file)

      const postContent: PostData = {
        frontMatter: {
          id: id,
          title: matterResult.data.title,
          date: matterResult.data.date,
          updatedDate: matterResult.data.updatedDate || null
        },
        content: matterResult.content
      }

      return postContent
    } catch (error) {
      throw error;
    }
  }

  static async isPostPage(id: Array<string>): Promise<boolean> {
    const idPath = path.join(...id)

    try {
      // Markdown post file
      const pathStats = await fs.promises.stat(
        path.join(this.postsDirectory, `${idPath}.md`)
      )

      if (pathStats.isFile()) {
        return true
      }
    } catch (error) {
      // If file not found it's a directory
      return false
    }
    return false
  }

  // method to get all posts frontmatter data from a directory
  // and to sort them by date
  static async getSortedPosts(id: Array<string>): Promise<Array<PostInfo>> {
    let i = 0
    // Recursively find all posts and return their PostFrontMatter
    const getPostFrontMattersInDirectory = async (directoryPath: string) => {
      const directoryDirents: Array<fs.Dirent> = await fs.promises.readdir(
        this.postsDirectory + directoryPath,
        { withFileTypes: true }
      )
      let postInfos: Array<PostInfo> = []
      for (let dirent of directoryDirents) {
        let category = ''
        if (i == 0) {
          const directoryPathArray = directoryPath.split('/')
          category = directoryPathArray[directoryPathArray.length - 1]
        }
        if (dirent.isDirectory()) {
          for (let frontMatter of
            await getPostFrontMattersInDirectory(directoryPath + '/' + dirent.name))
            postInfos.push(frontMatter)
        } else if (dirent.isFile()) {
          const idName = dirent.name.replace(/.md$/, '')
          const id = path.join(directoryPath, idName).split('/')
          const frontMatter = (await this.getPostData(id)).frontMatter
          postInfos.push({ frontMatter, category: category })
        }
      }
      return postInfos
    }

    const sortPosts = (a: PostInfo, b: PostInfo) => {
      if (a.frontMatter.date > b.frontMatter.date) {
        return -1
      } else if (a.frontMatter.date < b.frontMatter.date) {
        return 1
      } else {
        return 0
      }
    }

    try {
      const idPath = path.join(...id)
      const posts = await getPostFrontMattersInDirectory('/' + idPath)
      return posts.sort(sortPosts)
    } catch (error) {
      throw error
    }
  }
}

export default Posts
