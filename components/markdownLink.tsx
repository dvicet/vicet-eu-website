import Link from 'next/link';

const MarkdownLink = (props: any) => {
  const href = props.href
  const children = props.children
  // local link
  if (href.startsWith('/') || href === '') {
    return (
      <Link href={href}>
        {children}
      </Link>
    )
  } else if (href.startsWith('#')) {
    // same page link : don't need Nextjs 'Link'
    return (
      <a
        href={href}
      >
        {children}
      </a>
    )
  } else {
    // external link
    return (
      <a
        href={href}
        target="_blank"
        rel="noopener noreferrer"
      >
        {children}
      </a>
    )
  }
}

export default MarkdownLink
