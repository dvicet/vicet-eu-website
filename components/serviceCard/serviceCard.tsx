import styles from './serviceCard.module.scss'

interface props {
  name: string;
  description: string;
  url: string;
  color: string;
}

export default function ServiceCard(props: props) {
  return (
    <a href={props.url} className={styles.cardLink}>
      <div
        className={styles.container}
        style={{ background: props.color }}
      >
        <p className={styles.cardTitle}>{props.name}</p>
        <p>{props.description}</p>
      </div>
    </a >
  )
}

/*{props.logoImage &&
  <Image
    src={props.logoImage}
    alt='{props.name} logo'
    width={500}
    height={500}
  />
}*/
