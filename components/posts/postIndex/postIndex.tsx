import Head from 'next/head'
import Link from 'next/link'
import styles from './postIndex.module.scss'
import type { PostInfo } from '../../../lib/posts'

interface props {
  categoryTitle: string,
  posts: Array<PostInfo>
}

export default function PostComponent(
  props: props
) {
  const listLastPosts = props.posts.map((post: PostInfo) => {
    const idPath = post.frontMatter.id.join('/')

    return (
      <Link href={'/posts' + idPath} key={idPath}>
        <div>
          <p>{post.frontMatter.title}</p>
        </div>
      </Link>
    )
  })

  return (
    <div className={styles.container}>
      <Head>
        <title>{props.categoryTitle}</title>
      </Head>
      <h1 className={styles.mainTitle}>{props.categoryTitle}</h1>
      <div>
        <h2>Derniers articles :</h2>
        {listLastPosts}
      </div>
    </div>
  )
}
