import Head from 'next/head'
import styles from './post.module.scss'
import 'highlight.js/styles/monokai.css'
import React from 'react'

interface Props {
  title: string,
  date: string,
  content?: string
}

export default function PostComponent(
  props: Props
) {
  return (
    <div className={styles.container}>
      <Head>
        <title>{props.title}</title>
      </Head>
      <h1 className={styles.mainTitle}>{props.title}</h1>
      <p className={styles.postDate}>{new Date(props.date).toLocaleDateString()}</p>
      {props.content &&
        <div dangerouslySetInnerHTML={{ __html: props.content }} />
      }
    </div>
  )
}
