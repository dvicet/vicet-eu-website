import Head from 'next/head'
import styles from './layout.module.scss'
import Link from 'next/link'
import classnames from 'classnames'
import { useState } from 'react'

import { faEnvelope } from '@fortawesome/free-solid-svg-icons'
import { faLinkedin } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default function Layout({
  children
}: {
  children: React.ReactNode
}) {
  const [isActive, setActive] = useState(true)

  const toggleMenu = () => setActive((x) => !x)

  return (
    <div className={styles.container}>
      <Head>
        <link rel="icon" href="/favicon.ico" />
        <meta
          name="description"
          content="site de Damien"
        />
      </Head>
      <header className={styles.navHeader}>
        <div className={styles.siteTitle}>
          <Link href={`/`} title="Accueil du site">
            <p>le site de <span>Damien</span></p>
          </Link>
          <button aria-label="Menu" className={styles.toggleNavButton} onClick={toggleMenu}>
          </button>
        </div>
        <nav className={classnames({ [styles.hidden]: isActive === true, })}>
          <ul>
            <Link href={`/posts`}>
              <li>Ressources</li>
            </Link>
            <Link href={`/about`}>
              <li>À propos</li>
            </Link>
          </ul>
        </nav>
      </header>
      <div className={styles.pageContent}>
        <main>
          {children}
        </main>
      </div>
      <footer>
        <p>© le site de Damien - <a
          href="https://gitlab.com/dvicet/vicet-eu-website"
          target="_blank"
          rel="noopener noreferrer"
        >dépôt du code source</a></p>
        <p className={styles.contactTitle}>Contact</p>
        <a
          href="https://www.linkedin.com/in/damien-vicet-530899196"
          target="_blank"
          rel="noopener noreferrer"
          className={styles.contactLink}
        >
          <FontAwesomeIcon className={styles.icon} icon={faLinkedin} />
          <p>Mon profil LinkedIn</p>
        </a>
        <a
          href="mailto:contact@vicet.eu"
          className={styles.contactLink}
        >
          <FontAwesomeIcon className={styles.icon} icon={faEnvelope} />
          <p>{"M'envoyer un courriel"}</p>
        </a>

      </footer>
    </div>
  )
}

/*
<meta
  property="og:image"
  content={`https://og-image.vercel.app/${encodeURI(
    siteTitle
  )}.png?theme=light&md=0&fontSize=75px&images=https%3A%2F%2Fassets.zeit.co%2Fimage%2Fupload%2Ffront%2Fassets%2Fdesign%2Fnextjs-black-logo.svg`}
/>*/
//<meta name="og:title" content={siteTitle} />
//<meta name="twitter:card" content="summary_large_image" />
