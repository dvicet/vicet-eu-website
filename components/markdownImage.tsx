import Image from 'next/image';

const MarkdownImage = ({
  src, alt
}: {
  src: string, alt: string
}) => {
  return (
    <figure>
      <Image
        src={src}
        alt={alt}
        fill={true}
      />
      <figcaption>{alt}</figcaption>
    </figure>
  )
}

export default MarkdownImage
