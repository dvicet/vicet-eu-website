import Layout from '../../components/layout/layout'
import PostIndex from '../../components/posts/postIndex/postIndex'
import Posts from '../../lib/posts'
import { GetStaticProps } from 'next'

import React from 'react'

import type { PostInfo } from '../../lib/posts'

interface Props {
  posts: Array<PostInfo>,
  categoryTitle: string
}

export default function Post(props: Props) {
  return (
    <Layout>
      <PostIndex
        categoryTitle={props.categoryTitle}
        posts={props.posts}
      />
    </Layout>
  )
}

export const getStaticProps: GetStaticProps = async () => {
  // Category index page
  const posts = await Posts.getSortedPosts(['']);
  const category = 'Articles et tutoriels'
  return {
    props: {
      categoryTitle: category,
      posts
    }
  }
}
