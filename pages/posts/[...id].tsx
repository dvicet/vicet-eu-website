import Layout from '../../components/layout/layout'
import PostComponent from '../../components/posts/post/post'
import PostIndex from '../../components/posts/postIndex/postIndex'
import Posts from '../../lib/posts'
import { GetStaticProps, GetStaticPaths } from 'next'

import React from 'react'
import ReactDOMServer from 'react-dom/server'

import type { PostData } from '../../lib/posts'
import type { PostInfo } from '../../lib/posts'

import { unified } from 'unified'
import remarkParse from 'remark-parse'
import remarkGfm from 'remark-gfm'
import remarkToc from 'remark-toc'
import rehypeSlug from 'rehype-slug'
import remarkRehype from 'remark-rehype'
import rehypeRaw from 'rehype-raw'
import rehypeHighlight from 'rehype-highlight'
import rehypeStringify from 'rehype-stringify'
import rehypeReact from 'rehype-react'

// languages for highlight.js
import langPython from 'highlight.js/lib/languages/python'
import langBash from 'highlight.js/lib/languages/bash'
import langNginx from 'highlight.js/lib/languages/nginx'

import MarkdownLink from '../../components/markdownLink'
import MarkdownImage from '../../components/markdownImage'

interface Props {
  type: string,
  postData?: PostData,
  content?: string,
  posts?: Array<PostInfo>,
  categoryTitle?: string
}

export default function Post(props: Props) {
  if (props.type == "post" && props.postData) { // Post page
    return (
      <Layout>
        <PostComponent
          title={props.postData.frontMatter.title}
          date={props.postData.frontMatter.date}
          content={props.content}
        />
      </Layout>
    )
  } else if (props.posts && props.categoryTitle) { // Category page
    return (
      <Layout>
        <PostIndex
          categoryTitle={props.categoryTitle}
          posts={props.posts}
        />
      </Layout>
    )
  }
}

export const getStaticPaths: GetStaticPaths = async () => {
  const paths = await Posts.getAllPostIds()
  return {
    paths,
    fallback: false
  }
}

export const getStaticProps: GetStaticProps = async ({ params }) => {
  if (params) {
    const id = params.id as Array<string>
    // Post page
    if (await Posts.isPostPage(id)) {
      const postData = await Posts.getPostData(id)

      // languages for highlight.js
      const languages = {
        bash: langBash,
        python: langPython,
        nginx: langNginx
      }

      // Use remark to convert markdown into HTML string
      try {
        const processedContent = await unified()
          .use(remarkParse)
          .use(remarkGfm)
          .use(remarkToc, { heading: 'sommaire' })
          .use(remarkRehype)// , { allowDangerousHtml: true })
          // .use(rehypeRaw)
          .use(rehypeSlug)
          .use(rehypeHighlight, { languages: languages })
          .use(rehypeStringify)
          .use(rehypeReact, {
            createElement: React.createElement,
            components: {
              a: (props: Object) => React.createElement(MarkdownLink, props)
            }
          })
          .process(postData.content)

        return {
          props: {
            type: "post",
            postData: postData,
            content: ReactDOMServer.renderToString(processedContent.result)
          }
        }
      } catch (error) {
        throw error
      }
    }
    // Category index page
    const posts = await Posts.getSortedPosts(id);
    const category = id[id.length - 1].replace(/_/g, ' ')
    return {
      props: {
        type: "index",
        categoryTitle: category,
        posts
      }
    }
  }
  return {
    props: {}
  }
}
