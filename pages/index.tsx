import type { NextPage } from 'next'
import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Layout from '../components/layout/layout'
import ServiceCard from '../components/serviceCard/serviceCard'

const Home: NextPage = () => {
  return (
    <>
      <Head>
        <title>Accueil</title>
      </Head>
      <Layout>
        <h1>Services</h1>
        <section className={styles.servicesGrid}>
          <ServiceCard
            name='Nextcloud'
            description='Stockage de fichiers'
            url='https://nextcloud.vicet.eu'
            color='linear-gradient(-30deg, #56b9ff, #fafeff)'
          />
          <ServiceCard
            name='Orthogram'
            description='Jeu pour apprendre la grammaire des mots'
            url='https://orthogram.vicet.eu'
            color='linear-gradient(-30deg, #ffad56, #fffdfa)'
          />
          <ServiceCard
            name='Mailu'
            description='Serveur mail'
            url='https://mail.vicet.eu'
            color='linear-gradient(-30deg, #9496ff, #fafcff)'
          />
          <ServiceCard
            name='Matrix'
            description='Serveur de messagerie Matrix'
            url='https://matrix.vicet.eu'
            color='linear-gradient(-30deg, #8de47b, #fbfffa)'
          />
        </section>
      </Layout>
    </>
  )
}

export default Home
