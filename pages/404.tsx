import type { NextPage } from 'next'
import Head from 'next/head'
import Layout from '../components/layout/layout'
import Link from 'next/link'
import styles from '../styles/page404.module.scss'

const Page404: NextPage = () => {
  return (
    <>
      <Head>
        <title>Accueil</title>
      </Head>
      <Layout>
        <section className={styles.container}>
          <h1>Erreur 404 : Page non trouvée</h1>
          <p>{"Oups, la page que vous cherchez n'existe plus ou n'a jamais existé."}</p>
          <Link href={`/`}>
            <p>{"Retourner à l'accueil"}</p>
          </Link>
        </section>
      </Layout>
    </>
  )
}

export default Page404
