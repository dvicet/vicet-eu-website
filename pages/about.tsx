import type { NextPage } from 'next'
import Head from 'next/head'
import Layout from '../components/layout/layout'
// import styles from '../styles/About.module.css'

const About: NextPage = () => {
  return (
    <>
      <Head>
        <title>À propos</title>
      </Head>
      <Layout>
        <h1>À propos</h1>
        <p>Je suis principalement développeur back-end et administrateur systèmes. Ce site a pour but de réunir les services que j&apos;auto-héberge, et aussi de regrouper des articles sur comment installer ces applications ou sur d&apos;autres sujets qui m&apos;intéressent. Si vous avez des remarques ou des questions, n&apos;hésitez pas à me contacter !</p>
      </Layout>
    </>
  )
}

export default About
