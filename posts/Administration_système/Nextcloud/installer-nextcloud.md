---
title: 'Installer Nextcloud avec Docker'
date: '2022-08-25'
---

Dans ce tutoriel, je vais vous expliquer comment j'ai installé Nextcloud sur un serveur Linux à l'aide de Docker.

Note : Les explications qui vont suivre sont basées sur ma propre expérience. Ce n'est probablement pas une configuration parfaite et c'est améliorable. Cependant, j'ai fait de mon mieux pour que cette installation soit sécurisée et fonctionnelle.

À la fin de ce tutoriel, vous aurez ces différents services sur votre serveur :

* **Nextcloud**
* **Collabora Office** pour ouvrir les Documents de type LibreOffice dans votre navigateur et les éditer en collaboration avec d'autres utilisateurs.
* Le logiciel **Borg Backup** pour gérer vos sauvegardes

## Prérequis

Pour mettre en place tout cela il vous faudra :

### Matériel

* Un serveur pour vos services
  - Un viel ordinateur portable peut faire l'affaire
* Un serveur pour vos sauvegardes
  - Une petite Raspberry Pi avec un disque dur suffira
  - De préférence, stocké à un emplacement géographique différent du serveur. (imaginez votre maison brûle avec vos deux serveurs : vous perdez toutes vos données.)

### Autre

* Une distribution Linux installée (j'utilise Debian)
* Les ports 80 et 443 ouverts au niveau du pare-feu et redirigés vers votre serveur au niveau de votre routeur
* Deux domaines ou sous domaines pour Nextcloud et Collabora

## Sites officiels

Pour plus d'informations, n'hésitez pas à aller voir les ressources officielles :

* [Le dépot GitHub de l'image Docker Nextcloud officielle](https://github.com/nextcloud/docker)
  - Vous y trouverez plein d'exemples de configuration
* [la documentation de Nextcloud](https://docs.nextcloud.com/server/latest/admin_manual/)
* [Le site de Collabora Office](https://www.collaboraoffice.com/code/)
* [La documentation de Borg Backup](https://borgbackup.readthedocs.io/en/stable/)
* [La documentation de Docker](https://docs.docker.com/)

## Sommaire

## Préparation des domaines

Pour que vos services soient accessibles, vous devrez configurer votre zone DNS pour que vos domaines renvoient vers l'adresse IP de votre serveur.

Il vous faudra un domaine pour Nextcloud et un autre pour Collabora. Par exemple, si vous possédez `example.org`, vous pouvez créer le sous-domaine `nextcloud.example.org` pour Nextcloud et `office.example.org` pour Collabora. J'utiliserai ces nom de domaines en exemple.

N'oubliez pas de rediriger aussi vers votre adresse IPv6 (si vous en avez une) !

## Installation des paquets nécessaires

### Installation de Docker

Pour installer Docker, vous pouvez vous référer à la documentation de Docker pour votre distribution, ici pour Debian : [https://docs.docker.com/engine/install/debian/](https://docs.docker.com/engine/install/debian/)

La méthode recommandée est d'ajouter les dépots de Docker.


Aussi, vous aurez besoin du serveur web Nginx et de Certbot pour les certificats Let's Encrypt.  
Pour la plupart des distributions, vous pouvez installer respectivement les paquets `nginx` et `python-certbot`.

### Configuration de nginx

Pour que les conteneurs Nextcloud et Collabora soient accessibles sur le web, il faudra configurer nginx en reverse-proxy.

Tout d'abord, dans votre dossier `/etc/nginx/sites-available`, créez deux fichiers `nextcloud.conf` et `collabora-office.conf` avec ces contenus :

Remplacez bien `example.org` par votre domaine.

#### Nextcloud

```nginx
server {
    listen [::]:443 ssl http2;
    listen 443 ssl http2;

    server_name nextcloud.example.org;    

    location / {
        proxy_pass http://localhost:8080;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }

    client_max_body_size        10G;
    client_body_buffer_size     400M;
    
    add_header Strict-Transport-Security "max-age=15552000; includeSubDomains" always;    
    
    location /.well-known/carddav {
        return 301 $scheme://$host/remote.php/dav;
    }

    location /.well-known/caldav {
        return 301 $scheme://$host/remote.php/dav;
    }

    # logs
    access_log /var/log/nginx/nextcloud.access.log;
    error_log /var/log/nginx/nextcloud.error.log;
}

server {
    listen [::]:80;
    listen 80;

    server_name nextcloud.example.org;

    if ($host = nextcloud.example.org) {
        return 301 https://$host$request_uri;
    }
    return 404;
}
```

#### Collabora Office

```nginx
server {
    listen [::]:443 ssl;
    listen 443 ssl;

    server_name office.example.org;

    add_header Strict-Transport-Security "max-age=15552000; includeSubDomains" always;
    add_header X-Robots-Tag none;
    
    # static files
    location ^~ /browser {
      proxy_pass https://127.0.0.1:9980;
      proxy_set_header Host $http_host;
    }

    # WOPI discovery URL
    location ^~ /hosting/discovery {
      proxy_pass https://127.0.0.1:9980;
      proxy_set_header Host $http_host;
    }

    # Capabilities
    location ^~ /hosting/capabilities {
      proxy_pass https://127.0.0.1:9980;
      proxy_set_header Host $http_host;
    }

    # main websocket
    location ~ ^/cool/(.*)/ws$ {
      proxy_pass https://127.0.0.1:9980;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection "Upgrade";
      proxy_set_header Host $http_host;
      proxy_read_timeout 36000s;
    }

    # download, presentation and image upload
    location ~ ^/(c|l)ool {
      proxy_pass https://127.0.0.1:9980;
      proxy_set_header Host $http_host;
    }

    # Admin Console websocket
    location ^~ /cool/adminws {
      proxy_pass https://127.0.0.1:9980;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection "Upgrade";
      proxy_set_header Host $http_host;
      proxy_read_timeout 36000s;
    }
    
    # logs
    access_log /var/log/nginx/collabora.access.log;
    error_log /var/log/nginx/collabora.error.log;
}

server {
    listen [::]:80;    
    listen 80;

    server_name office.example.org;

    if ($host = office.example.org) {
        return 301 https://$host$request_uri;
    }
    return 404;
}
```

Faites ensuite un lien symbolique pour chaque fichier vers le dossier `/etc/nginx/sites-enabled` :

```bash
ln -s /etc/nginx/site-available/nextcloud.conf /etc/nginx/site-enabled/

ln -s /etc/nginx/site-available/collabora-office.conf /etc/nginx/site-enabled/
```

Et rechargez nginx :

```bash
systemctl reload nginx
```

Si vous essayez de rentrer votre domaine dans le navigateur, il devrait vous renvoyer une erreur de sécurité car le certificat Let's Encrypt n'a pas encore été ajouté.

Pour installer le certificat, vous pouvez lancer la commande suivante :

```bash
certbot --nginx
```

Il vous faudra ensuite choisir les domaines pour lequels vous voulez activer HTTPS (avec la redirection pour HTTP).

Voilà, nginx est maintenant configuré pour accueillir Nextcloud et Collabora !

Si vous essayez de rentrer vos domaines dans le navigateur, vous devriez avoir une erreur `Bad gateway`. C'est normal car vous n'avez pas encore démarré vos conteneurs Docker.

## Déploiement avec docker-compose

Tout d'abord, sur votre serveur, créez un dossier nommé `nextcloud` (dans votre dossier home par exemple) qui va contenir tous vos fichiers de configuration pour vos services.

Attention, il faudra par la suite que vous preniez soin faire une sauvegarde de ce dossier à un endroit sûr, ailleurs que sur votre serveur.

Créez ensuite un fichier `docker-compose.yml` avec ce contenu :

```yaml
version: '3'

volumes:
  nextcloud:
  db:
  data:

networks:
  nextcloud:

services:
  db:
    image: mariadb:10.5
    container_name: mariadb_nextcloud
    command: --transaction-isolation=READ-COMMITTED --binlog-format=ROW
    restart: always
    networks:
      - nextcloud
    volumes:
      - db:/var/lib/mysql
    environment:
      - MYSQL_ROOT_PASSWORD=${DB_ROOT_PASSWORD}
      - MYSQL_PASSWORD=${DB_PASSWORD}
      - MYSQL_DATABASE=nextcloud
      - MYSQL_USER=nextcloud

  redis:
    image: redis:alpine
    container_name: redis_nextcloud
    networks:
      - nextcloud
    restart: always

  app:
    image: nextcloud:24-fpm-alpine
    container_name: nextcloud
    depends_on:
      - db
      - redis
    networks:
      - nextcloud
    volumes:
      - nextcloud:/var/www/html
      - data:/var/www/data
    restart: always
    environment:
      - NEXTCLOUD_DATA_DIR="/var/www/data"
      - NEXTCLOUD_TRUSTED_DOMAINS=${NEXTCLOUD_URL}
      - TRUSTED_PROXIES=127.0.0.1
      - OVERWRITEHOST=${NEXTCLOUD_URL}
      - OVERWRITEPROTOCOL=https
      - REDIS_HOST=redis

  web:
    build: ./web
    container_name: web_nextcloud
    restart: always
    ports:
      - 8080:80
    volumes:
      - nextcloud:/var/www/html:ro
    depends_on:
      - app
    networks:
      - nextcloud

  cron:
    image: nextcloud:24-fpm-alpine
    container_name: cron_nextcloud
    restart: always
    volumes:
      - nextcloud:/var/www/html
      - data:/var/www/data
    entrypoint: /cron.sh
    depends_on:
      - db
      - redis
    networks:
      - nextcloud

  office:
    image: collabora/code
    container_name: collabora_nextcloud
    cap_add:
      - MKNOD
    ports:
      - 9980:9980
    restart: always
    environment:
      - aliasgroup1=https://${NEXTCLOUD_URL}:443

  metrics:
    image: xperimental/nextcloud-exporter
    container_name: metrics_nextcloud
    ports:
      - 9205:9205
    environment:
      NEXTCLOUD_SERVER: https://${NEXTCLOUD_URL}
      NEXTCLOUD_AUTH_TOKEN: ${NEXTCLOUD_AUTH_TOKEN}
```

Créez aussi un fichier `.env`, qui va remplacer les variables d'environnement dans votre fichier docker-compose :

```
NEXTCLOUD_URL=nextcloud.example.org
DB_ROOT_PASSWORD=**your-password**
DB_PASSWORD=**your-password**
```

Pour générer les mots de passe de votre base de données, vous pouvez utiliser la commande `pwgen`, par exemple :

```bash
pwgen -1s 20
```

Sans rentrer dans les détails, ce fichier de configuration permettra de créer 4 conteneurs :

* Une **base de données Mariadb**, pour stocker les données de Nextcloud.
* Une **base de donnéed Redis**, permettant de mettre en cache des données pour améliorer les performances
* Un conteneur pour **Nextcloud**.
  * Sa version majeure est fixée pour éviter des problèmes de mises à jour automatique. Lors d'une nouvelle mise à jour de Nextcloud, vous devrez changer manuellement cette version. *(Remplacer ici `24` par `25` par exemple)*
* Un conteneur avec **Nginx** qui fait office de serveur web pour générer les pages php de Nextcloud
* Un conteneur pour exécuter régulièrement des tâches avec **cron**
* Un conteneur pour **Collabora**
* Un conteneur *metrics* pour exporter des statistiques qui peuvent être utilisés avec **Prometheus** pour le monitoring de votre serveur. *(facultatif)*

De plus, 3 volumes sont créés (dans `/var/lib/docker/volumes`) pour Mariadb, Nextcloud et un volume séparé pour les données des utilisateurs de Nextcloud.

Vous allez maintenant pouvoir déployer vos services avec la comande :

```bash
docker-compose up -d
```

L'option `-d` (pour `detach`) permet d'exécuter la commande en arrière plan.

Normalement en tapant la commande `docker ps`, vous devriez voir que vos conteneurs sont en fonctionnement.

Pour arrêter les services vous pouvez simplement lancer :

```bash
docker-compose stop
```

## Installation de Nextcloud

Une fois les services lancés, vous devriez pouvoir accéder à l'interface d'installation de Nextcloud avec votre navigateur en vous rendant à l'adresse `nextcloud.example.org`.

![Interface d'installation web de Nextcloud](/images/tuto-installer-nextcloud/web-install.png)

L'interface web d'installation de Nextcloud

Renseignez un nom d'utilisateur et un mot de passe pour votre compte d'administrateur.

Pour le répertoire des données entrez `/var/www/data`.

Dans _Configurer la base de données_, mettez `nextcloud` comme nom d'utilisateur et nom de base de donnée, reportez le mot de passe précédemment créé (`DB_PASSWORD`), et mettez `db` comme nom d'hôte.

Je vous conseille de décocher _Installer les applications recommandées_ pour choisir manuellement vos applications plus tard.

Enfin, cliquez sur **Terminer l'installation**

## Configuration après installation

Félicitations, vous avez installé Nextcloud.

### Vérification de la configuration

En cliquant en haut à droite comme sur l'image suivante, vous avez accès à plusieurs paramètres.

![Menu de Nextcloud](/images/tuto-installer-nextcloud/nextcloud-menu.png)

Le menu des paramètres

En cliquant sur **Paramètres**, vous avez accès au menu d'**administration** à gauche.

Dans **Vue d'ensemble**, vous pouvez voir les problèmes détectés automatiquement sur votre serveur et les conseils pour les résoudre.

### Intégration de Collabora Office à Nextcloud

Retournez au menu des pamètres et cliquez sur **Applications**. Recherchez **Collabora Online** en cliquant sur la loupe en haut. Puis cliquez sur **Télécharger et activer**

![Installation de Collabora dans Nextcloud](/images/tuto-installer-nextcloud/install-collabora.png)

Installation de l'Application Collabora dans Nextcloud

Retournez maintenant dans les paramètres d'Administration, vous devriez avoir accès aux paramètres de Collabora. Cochez la case **"Utiliser votre propre serveur"** et rentrez le domaine de votre Collabora Office.

![Paramètres de Collabora](/images/tuto-installer-nextcloud/collabora-settings.png)

Paramètres de Collabora

Vous pouvez maintenant retourner dans les fichiers et créer un document de type LibreOffice. Si vous cliquez dessus pour l'ouvrir, Collabora Office devrait s'ouvrir dans votre Nextcloud.

![Créer un nouveau document ODT](/images/tuto-installer-nextcloud/new-file.png)

Créer un nouveau document ODT

## Gérez vos sauvegardes avec Borg

Pour sécuriser votre installation et éviter des pertes de données. Il est important de mettre en place des **sauvegardes**. Dans ce tutoriel, nous allons utiliser **Borg Backup**. C'est un outil de sauvegarde en ligne de commande très simple à utiliser.

Tout d'abord, installez le paquet `borgbackup` sur votre serveur Nextcloud **et** votre serveur de sauvegarde. Vous devrez ensuite créer un dépot pour vos sauvegardes. Vous pouvez exécuter la commande suivante sur votre serveur :

```bash
borg init --encryption=repokey /path/to/repo
```

Vous devrez ensuite entrer une phrase de passe pour chiffrer vos sauvegardes, vous pouvez en générer une préalablement avec :

```bash
pwgen -1s 50
```

**Attention, vous devrez sauvegarder cette phrase de passe à un endroit sûr (ne la gardez pas uniquement sur votre serveur Nextcloud).**

Nous allons ensuite décomposer nos sauvegardes en deux scripts shell que nous allons créer dans notre dossier `nextcloud`

Créez un fichier `nextcloud_borg_backup.sh` avec le contenu suivant :

_Je me suis basé sur l'exemple de script dans la documentation de Borg._

```bash
#!/bin/sh

# Setting this, so the repo does not need to be given on the commandline:
export BORG_REPO=$BORG_REPO

# See the section "Passphrase notes" for more infos.
export BORG_PASSPHRASE=$BORG_PASSPHRASE

# some helpers and error handling:
info() { printf "\n%s %s\n\n" "$( date )" "$*" >&2; }
trap 'echo $( date ) Backup interrupted >&2; exit 2' INT TERM

info "Starting backup"

# Backup the most important directories into an archive named after
# the machine this script is currently running on:

borg create                        \
   --verbose                       \
   --filter AME                    \
   --list                          \
   --stats                         \
   --show-rc                       \
   --compression lz4               \
                                   \
   ::'{hostname}-{now}'            \
   /var/lib/docker/volumes/nextcloud_nextcloud \
   /var/lib/docker/volumes/nextcloud_data \
   $TEMP_DB_BACKUP \

backup_exit=$?

info "Pruning repository"

# Use the `prune` subcommand to maintain 7 daily, 4 weekly and 6 monthly
# archives of THIS machine. The '{hostname}-' prefix is very important to
# limit prune's operation to this machine's archives and not apply to
# other machines' archives also:

borg prune                          \
   --list                          \
   --prefix '{hostname}-'          \
   --show-rc                       \
   --keep-daily    7               \
   --keep-weekly   4               \
   --keep-monthly  6               \

prune_exit=$?

# use highest exit code as global exit code
global_exit=$(( backup_exit > prune_exit ? backup_exit : prune_exit ))

if [ ${global_exit} -eq 0 ]; then
   info "Backup and Prune finished successfully"
elif [ ${global_exit} -eq 1 ]; then
   info "Backup and/or Prune finished with warnings"
else
   info "Backup and/or Prune finished with errors"
fi

exit ${global_exit}
```

Créez ensuite un autre fichier nommé `nextcloud_backup.sh` :

```bash
#!/bin/bash

endBackup(){
  # Disable maintenance mode
  docker-compose exec -T --user www-data app php occ maintenance:mode --off
  if [ $? -ne 0 ]; then
    exit 4;
  fi

  # Remove database dump
  rm $TEMP_DB_BACKUP
}

# Enable maintenance mode
docker-compose exec -T --user www-data app php occ maintenance:mode --on
if [ $? -ne 0 ]; then
  exit 1;
fi

# database dump
docker-compose exec -T db sh -c 'exec mysqldump nextcloud -u nextcloud -p'$DB_PASSWORD > $TEMP_DB_BACKUP
if [ $? -ne 0 ] && [ -e $TEMP_DB_BACKUP ]; then
  exit 2;
fi

# borg backup
./nextcloud_borg_backup.sh
if [ $? -ne 0 ]; then
  echo "[$(date)]" "Borg backup failed"
  endBackup
  exit 3;
fi

endBackup
```

Ce script va réaliser les étapes suivantes :

* Met Nextcloud en mode maintenance.
* Crée une sauvegarde de la base de données.
* Sauvegarde la base de données et les fichiers de Nextcloud avec borg.
* Enlève le mode maintenance

Modifiez votre fichier `.env` en ajoutant les lignes suivantes

```
TEMP_DB_BACKUP=/path/to/temp/nextcloud_db_backup.sql
BORG_REPO=ssh://user@backup_server.example.org:PORT/path/to/borg/repo
BORG_PASSPHRASE=your passphrase
```

* `TEMP_DB_BACKUP` : Un dossier qui contiendra provisoirement la sauvegarde de votre base de données.
* `BORG_REPO` : L'emplacement de votre dépot borg.
* `BORG_PASSPHRASE` : Votre phrase de passe borg.

## Sauvegardez et mettez à jour automatiquement avec Cron

Un troisième script appelé `nextcloud_backup_and_update.sh` va nous permettre de sauvegarder et mettre à jour vos services :

```bash
#!/bin/bash

cd $(dirname $0)

export $(cat .env | xargs)

# Backup Files and Database with borg
./nextcloud_backup.sh
if [ $? -ne 0 ]; then
    exit 1;
fi
echo "[$(date)]" "Files backed up successfully"

# Update Nextcloud
docker-compose pull
docker-compose build --pull
docker-compose up -d
if [ $? -ne 0 ]; then
    exit 2;
fi
echo "[$(date)]" "Nextcloud updated"
```

Vous pouvez lancer ce script en superutilisateur pour vérifier qu'il fonctionne correctement.

Installez ensuite `cron` s'il n'est pas déjà présent sur votre système. Puis lancez la commande :

```bash
sudo crontab -e
```

Insérez la ligne suivante dans votre fichier crontab, remplacant par le chemin d'accès vers votre dossier nextcloud :

```bash
0 3 * * * /path/to/nextcloud/nextcloud_backup_and_update.sh > /path/to/nextcloud/last_backup.log 2>&1
```

Cela exécutera le script `nextcloud_backup_and_update.sh` chaque nuit à 3h pour sauvegarder et mettre à jour vos services automatiquement et renverra les logs vers `last_backup.log`. Soyez libre de changer l'heure qui vous convient. Pour tester vous pouvez commencer par programmer l'exécution du script dans la prochaine minute.

## Maintenez votre serveur

Félicitations ! Vous avez désormais un serveur Nextcloud sécurisé, avec des sauvegardes automatiques programmées. Dans ce tutoriel, il manquerait peut-être la partie monitoring pour surveiller votre serveur et être averti en cas de problème. Mais Nextcloud fournit déjà beaucoup d'outils permettant de surveiller que tout fonctionne correctement. Allez régulièrement dans les paramètres administrateur voir la vue d'ensemble, les logs, etc ... Pensez à mettre à jour vos Applications. Si la vue d'ensemble vous notifie d'une mise à jour, ne la faites pas, elle sera faite automatiquement au redémarrage du conteneur.

Ce petit script shell que vous pouvez renommer `occ.sh` vous permettra d'exécuter plus facilement les commandes occ permettant de faire des opérations de maintenance pour Nextcloud :

```bash
#!/bin/bash
sudo docker-compose exec --user www-data app php occ $*
```

Grâce à Nextcloud, vous pourrez bénéficier de nombreux services qui vous permettront d'être maître de vos données !
