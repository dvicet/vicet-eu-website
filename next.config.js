/** @type {import('next').NextConfig} */
module.exports = {
  reactStrictMode: true,
}

// sass and scss config
const path = require('path')

module.exports = {
  sassOptions: {
    includePaths: [path.join(__dirname, 'styles')],
  },
}
